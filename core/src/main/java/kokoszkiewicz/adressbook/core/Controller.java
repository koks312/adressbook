package kokoszkiewicz.adressbook.core;

import kokoszkiewicz.adressbook.core.dao.ContactDao;
import kokoszkiewicz.adressbook.core.dao.UserDao;
import kokoszkiewicz.adressbook.model.Contact;
import kokoszkiewicz.adressbook.model.User;

import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 * Created by Emil on 2016-07-06.
 */
@Stateless
public class Controller {

    @EJB
    ContactDao contactDao;

    @EJB
    UserDao userDao;

    public User login(String login, String password){
        User user = userDao.find(login);
        if(user==null || !password.equals(user.getPassword())) return null;
        return user;
    }

    public boolean register(String login, String password){
        if(userDao.find(login)!=null) return false;
        User user = new User();
        user.setLogin(login);
        user.setPassword(password);
        userDao.save(user);
        return true;
    }

    public boolean addContact(User user, String forename, String surname, long homePhone, long businessPhone, String email){
        Contact newContact = new Contact();
        newContact.setForename(forename);
        newContact.setSurname(surname);
        newContact.setHomePhone(homePhone);
        newContact.setBusinessPhone(businessPhone);
        newContact.setEmail(email);
        User _user = userDao.find(user.getLogin());
        newContact.setOwner(_user);
        _user.addContact(newContact);
        user.addContact(newContact);
        userDao.merge(_user);
        return true;
    }

    public void remove(Contact contact){
        contact.setDeleted(true);
        userDao.merge(contact.getOwner());
    }
}
