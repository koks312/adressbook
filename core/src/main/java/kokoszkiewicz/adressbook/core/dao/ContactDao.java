package kokoszkiewicz.adressbook.core.dao;

import kokoszkiewicz.adressbook.model.Contact;
import kokoszkiewicz.adressbook.model.Contact_;
import kokoszkiewicz.adressbook.model.User;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Emil on 2016-07-06.
 */
@Stateless
public class ContactDao{

    @PersistenceContext
    protected EntityManager em;

    public Contact merge(Contact contact){
        em.merge(contact);
        return contact;
    }

    public Contact save(Contact contact){
        em.persist(contact);
        return contact;
    }
}
