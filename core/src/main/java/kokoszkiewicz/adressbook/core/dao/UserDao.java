package kokoszkiewicz.adressbook.core.dao;

import kokoszkiewicz.adressbook.model.User;
import kokoszkiewicz.adressbook.model.User_;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Stateless
public class UserDao{

    @PersistenceContext
    EntityManager em;

    public User save(User user){
        em.persist(user);
        return user;
    }

    public User merge(User user){
        em.merge(user);
        return user;
    }

    public User find(String login){
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<User> cq = cb.createQuery(User.class);
        Root<User> root = cq.from(User.class);
        cq.select(root).where(cb.equal(root.get(User_.login), login));
        List resultList= em.createQuery(cq).getResultList();
        return resultList.size()>0?(User) resultList.get(0):null;
    }
}