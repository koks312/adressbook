package kokoszkiewicz.adressbook.web;

import kokoszkiewicz.adressbook.core.Controller;
import kokoszkiewicz.adressbook.model.User;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.view.facelets.FaceletContext;
import java.io.IOException;

/**
 * Created by Emil on 2016-07-07.
 */
@ManagedBean
@SessionScoped
public class LoginBean {

    private boolean showErrorMessage=false;

    @EJB
    Controller controller;

    private boolean isUserLogged=false;

    private User currentUser;

    private String login;
    private String password;

    public void doLogin(){
        if(login==null || password ==null){
            showErrorMessage=true;
            return;
        }
        currentUser=controller.login(login, password);
        if(currentUser!=null)
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("/adressbook");
            showErrorMessage=false;
            isUserLogged=true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        else showErrorMessage=true;
    }

    public void doLogout(){
        currentUser=null;
        isUserLogged=false;
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("/adressbook");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean isUserLogged() {
        return isUserLogged;
    }

    public void setUserLogged(boolean userLogged) {
        isUserLogged = userLogged;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isShowErrorMessage() {
        return showErrorMessage;
    }

    public void setShowErrorMessage(boolean showErrorMessage) {
        this.showErrorMessage = showErrorMessage;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public void checkPermission(ComponentSystemEvent event){
        if(!isUserLogged)
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("/adressbook");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
