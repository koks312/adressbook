package kokoszkiewicz.adressbook.web;

import kokoszkiewicz.adressbook.core.Controller;
import kokoszkiewicz.adressbook.model.Contact;
import org.primefaces.model.LazyDataModel;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import java.util.List;

/**
 * Created by Emil on 2016-07-06.
 */
@ManagedBean
@SessionScoped
public class ViewContactsBean {

    @EJB
    private Controller controller;

    public void remove(Contact contact){
        controller.remove(contact);
    }
}
