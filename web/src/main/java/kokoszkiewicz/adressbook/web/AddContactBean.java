package kokoszkiewicz.adressbook.web;

import kokoszkiewicz.adressbook.core.Controller;
import kokoszkiewicz.adressbook.model.User;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

/**
 * Created by Emil on 2016-07-07.
 */

@ManagedBean
public class AddContactBean {

    @EJB
    Controller controller;

    @ManagedProperty(value = "#{loginBean}")
    LoginBean loginBean;

    private String forename;
    private String surname;
    private String homePhone;
    private String businessPhone;
    private String email;

    private String message;
    private boolean showMessage=false;


    public void add(){
        try{
            long homeNumber = 0;
            long businessNumber = 0;
            if(!("".equals(homePhone))) homeNumber=Long.parseLong(homePhone);
            if(!("".equals(businessPhone))) businessNumber=Long.parseLong(businessPhone);
            if(((forename!=null && !(forename.equals(""))) || (surname!=null && !(surname.equals("")))) && controller.addContact(loginBean.getCurrentUser(), forename, surname, homeNumber, businessNumber, email))
                message = "Pomyślnie dodano kontakt";
            else message = "Nie udało się dodać kontaktu";
            showMessage=true;
        }catch (NumberFormatException e){
            message = "Nie udało się dodać kontaktu";
            showMessage=true;
        }

    }

    public String getForename() {
        return forename;
    }

    public void setForename(String forename) {
        this.forename = forename;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getHomePhone() {
        return homePhone;
    }

    public void setHomePhone(String homePhone) {
        this.homePhone = homePhone;
    }

    public String getBusinessPhone() {
        return businessPhone;
    }

    public void setBusinessPhone(String businessPhone) {
        this.businessPhone = businessPhone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LoginBean getLoginBean() {
        return loginBean;
    }

    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isShowMessage() {
        return showMessage;
    }

    public void setShowMessage(boolean showMessage) {
        this.showMessage = showMessage;
    }
}
