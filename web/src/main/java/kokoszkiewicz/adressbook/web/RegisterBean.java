package kokoszkiewicz.adressbook.web;

import kokoszkiewicz.adressbook.core.Controller;
import kokoszkiewicz.adressbook.core.dao.UserDao;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;

/**
 * Created by Emil on 2016-07-07.
 */
@ManagedBean
public class RegisterBean {
    private String login;
    private String password;
    private String confirmPassword;

    @EJB
    Controller controller;

    private String message;

    private boolean showMessage = false;

    public void register(){
        if(password!=null && password.equals(confirmPassword)) {
            showMessage = true;
            if (controller.register(login, password)) message="Udało się zarejestrować";
            else message="Nie udało się zarejestrować";
        }
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isShowMessage() {
        return showMessage;
    }

    public void setShowMessage(boolean showMessage) {
        this.showMessage = showMessage;
    }
}
