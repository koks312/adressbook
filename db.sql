CREATE TABLE [USERS](
	[id] [int] NOT NULL PRIMARY KEY,
	[login] [nvarchar](250) NOT NULL,
	[password] [nvarchar](250) NULL,
)

CREATE TABLE [CONTACT](
	[id] [int] NOT NULL,
	[forename] [nvarchar](250) NOT NULL,
	[surname] [nvarchar](250) NULL,
	[email] [varchar](250) NULL,
	[home_phone] [int] NULL,
	[business_phone] [int] NULL,
	[photo] [varchar](250) NULL,
	[owner_id] [int] NOT NULL,
	[deleted] [bit] NOT NULL,
)

ALTER TABLE [CONTACT]  WITH CHECK ADD FOREIGN KEY([owner_id])
REFERENCES [USERS] ([id])

CREATE SEQUENCE SEQ_USER
MINVALUE 1
START WITH 1
INCREMENT BY 1
CACHE 10

CREATE SEQUENCE SEQ_CONTACT
MINVALUE 1
START WITH 1
INCREMENT BY 1
CACHE 10