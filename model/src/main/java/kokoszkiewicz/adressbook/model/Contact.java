package kokoszkiewicz.adressbook.model;

import javax.persistence.*;

/**
 * Created by Emil on 2016-07-06.
 */

@Entity
@Table(name = "CONTACT")
public class Contact {

    @Id
    @SequenceGenerator(name = "SeqContact", sequenceName = "SEQ_CONTACT")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SeqContact")
    @Column(name="id", nullable = false)
    private long id;

    @Column(name = "forename", nullable = false)
    private String forename;

    @Column(name = "surname")
    private String surname;

    @Column(name = "email")
    private String email;

    @Column(name = "home_phone")
    private long homePhone;

    @Column(name = "business_phone")
    private long businessPhone;

    @ManyToOne(fetch=FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinColumn(name = "owner_id", nullable = false)
    private User owner;

    @Column(name ="deleted")
    private boolean deleted;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getForename() {
        return forename;
    }

    public void setForename(String forename) {
        this.forename = forename;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getHomePhone() {
        return homePhone;
    }

    public void setHomePhone(long homePhone) {
        this.homePhone = homePhone;
    }

    public long getBusinessPhone() {
        return businessPhone;
    }

    public void setBusinessPhone(long businessPhone) {
        this.businessPhone = businessPhone;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Contact contact = (Contact) o;

        if (id != contact.id) return false;
        if (homePhone != contact.homePhone) return false;
        if (businessPhone != contact.businessPhone) return false;
        if (deleted != contact.deleted) return false;
        if (forename != null ? !forename.equals(contact.forename) : contact.forename != null) return false;
        if (surname != null ? !surname.equals(contact.surname) : contact.surname != null) return false;
        if (email != null ? !email.equals(contact.email) : contact.email != null) return false;
        return owner != null ? owner.equals(contact.owner) : contact.owner == null;

    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (forename != null ? forename.hashCode() : 0);
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (int) (homePhone ^ (homePhone >>> 32));
        result = 31 * result + (int) (businessPhone ^ (businessPhone >>> 32));
        result = 31 * result + (owner != null ? owner.hashCode() : 0);
        result = 31 * result + (deleted ? 1 : 0);
        return result;
    }
}
